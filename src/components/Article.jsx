import React, { Component, PureComponent } from 'react';
class Article extends PureComponent {
    constructor(props) {
        super(props);
        this.state = {
            isOpen: props.defaultOpen,
            count: 0
        };
    }

    // порядок вызова методов
    // 1
    componentWillMount() {
        console.log('--->', 'mounting');
    }

    // 2 - позволяет перестраивать DOM только в случае необходимости 
    // (иначе он перестраивается не только для 1 и последней статьи, а для всех)
    // но лучше использовать PureComponent вместо Component (внутри реализовано)
    // souldComponentUpdate(nextProps, nextState) {
    //    return this.state.isOpen !== nextState.isOpen;
    //}

    // 3
    componentWillReceiveProps(nextProps) {
        console.log('--->', 'Will Receive Props');
        if (nextProps.defaultOpen !== this.props.defaultOpen) this.setState({
            isOpen: nextProps.defaultOpen
        });
    }

    // 4
    componentWillUpdate() {
        console.log('--->', 'Will Update');
    }
    
    // 5
    render() {
        const { article } = this.props;
        console.log('--->', this.props);
        const body = this.state.isOpen && <section className="cart-text">{ article.text }</section>;
        return (
            <div onClick={this.incrementCounter} className="card p-2 mx-auto" style={{width: '50%'}}>
                <div className="card-header">
                    <h2>
                    { article.title } ({this.state.count})
                        <button className="btn btn-secondary btn-lg float-end" onClick={ this.handleClick }>
                            { this.state.isOpen ? 'Close' : 'Open' }
                        </button>
                    </h2>
                </div>
                <div className="card-body">
                    
                    <h6 className="card-subtitle text-muted">Creation date: {(new Date(article.date).toDateString())}</h6> 
                    { body }
                </div>
            </div>
        )
    }

    incrementCounter = () => {
        this.setState({
            count: this.state.count + 1
        }) 
    }

    handleClick = () => {
        this.setState({
            isOpen: !this.state.isOpen
        })
    }

    // 6
    // componentDidUpdate(prevProps, prevState) {}

    // 7 - delete
    // componentWillUnmount() {}
}

export default Article
