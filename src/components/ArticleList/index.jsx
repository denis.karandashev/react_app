import React, {PureComponent} from 'react';
import Article from '../Article';
import './style.css';

export default class ArticleList extends PureComponent {
    render() {
        const articleElements = this.props.articles.map((article, index) =>
            <li key = {article.id} className="article-list__li">
                <Article article = {article} defaultOpen = {index === 0}/>
            </li>
        )
        return(
            <ul>
                { articleElements }
            </ul>
        )
    }
}

// export default function ArticleList({ articles }) {
//     // добавить key, уникальный в пределах передаваемого массива,
//     // тут это может быть article.id (но не индекс, т.к. при удалении они могут 
//     // поменяться), title (если он уникальный) и т.п.
//     const articleElements = articles.map((article, index) =>
//         <li key = {article.id} className="article-list__li">
//             <Article article = {article} defaultOpen = {index === 0}/>
//         </li>
//     )
//     return(
//         <ul>
//             { articleElements }
//         </ul>
//     )
// }